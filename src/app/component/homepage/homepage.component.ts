import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  allBooks: any[];

  constructor() { }

  ngOnInit(): void {
    this.allBooks = JSON.parse(localStorage.getItem('allBooks')).reverse();
  }

  search(value: any) {
    this.allBooks = JSON.parse(localStorage.getItem('allBooks')).reverse().filter(res => res.bookTitle.toLowerCase().includes(value.toLowerCase()));
  }
}
