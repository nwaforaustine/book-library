import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  time: any;
  noOfBooks = JSON.parse(localStorage.getItem('allBooks')).length;

  constructor() { }

  ngOnInit(): void {
    this.timeFunction();
  }

  timeFunction() {
    setInterval(() => {
      this.time = {hour: new Date().getHours(), min: new Date().getMinutes()};
    }, 1000);
  }

}
