import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.scss']
})
export class AddBookComponent implements OnInit {
  bookData: FormGroup;
  isEditing: boolean;
  bookId: number;
  updatingData: any;

  constructor(
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.bookData = this.fb.group({
      bookImage: ['', [Validators.compose([Validators.required, Validators.pattern('^https{0,1}:\\/\\/.+\\..+')])]],
      bookTitle: ['', [Validators.compose([Validators.required, Validators.minLength(5)])]],
      bookDescription: ['', [Validators.compose([Validators.required, Validators.minLength(100)])]],
      id: [0]
    });

    if (this.activatedRoute.snapshot.queryParams.id) {
      this.isEditing = true;
      this.bookId = this.activatedRoute.snapshot.queryParams.id;
      const data = JSON.parse(localStorage.getItem('allBooks'));
      this.updatingData = data[this.bookId];

      this.bookData.controls.bookImage.setValue(this.updatingData.bookImage);
      this.bookData.controls.bookTitle.setValue(this.updatingData.bookTitle);
      this.bookData.controls.bookDescription.setValue(this.updatingData.bookDescription);
      this.bookData.controls.id.setValue(this.updatingData.id);
    } else {
      this.isEditing = false;
    }
  }

  save() {
    if (this.isEditing){
      const allBooks = JSON.parse(localStorage.getItem('allBooks'));
      allBooks[this.updatingData.id] = this.bookData.value;
      localStorage.setItem('allBooks', JSON.stringify(allBooks));
    } else {
      if (this.bookData.valid) {
      const allBooks = JSON.parse(localStorage.getItem('allBooks'));
      const newBookList: any[] = allBooks;
      if (allBooks) {
        this.bookData.value.id = allBooks[allBooks.length - 1].id + 1;
        newBookList.push(this.bookData.value);
        localStorage.setItem('allBooks', JSON.stringify(newBookList));
      } else {
        localStorage.setItem('allBooks', JSON.stringify([this.bookData.value]));
      }
    }
    }
    this.router.navigate(['/admin/all-books']).then();
  }
}
