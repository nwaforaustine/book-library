import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {findIndex} from 'rxjs/operators';

@Component({
  selector: 'app-all-books',
  templateUrl: './all-books.component.html',
  styleUrls: ['./all-books.component.scss']
})
export class AllBooksComponent implements OnInit {
  displayedColumns: string[] = ['bookTitle', 'bookImage', 'bookDescription', 'action'];
  allBooks: any;




  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  ngOnInit() {
    this.allBooks = new MatTableDataSource<any>( JSON.parse(localStorage.getItem('allBooks')).reverse());
    this.allBooks.paginator = this.paginator;
    console.log(localStorage.getItem('allBooks'));
  }

  deleteBook(id: any) {
    if (JSON.parse(localStorage.getItem('allBooks')).length === 0) {
      localStorage.setItem('allBooks', null);
      this.allBooks = new MatTableDataSource<any>( JSON.parse(localStorage.getItem('allBooks')).reverse());
    } else {
      const data: any[] = JSON.parse(localStorage.getItem('allBooks'));
      console.log(id);
      console.log(data);
      const index: number = data.findIndex(res => res === id);
      data.splice(index, 1);
      localStorage.setItem('allBooks', JSON.stringify(data));
      this.allBooks = new MatTableDataSource<any>( JSON.parse(localStorage.getItem('allBooks')).reverse());
      this.allBooks.paginator = this.paginator;
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.allBooks.filter = filterValue.trim().toLowerCase();
  }
}

