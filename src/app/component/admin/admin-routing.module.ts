import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TemplateComponent} from './template/template.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AddBookComponent} from './add-book/add-book.component';
import {AllBooksComponent} from './all-books/all-books.component';

const route: Routes = [
  {
    path: '',
    component: TemplateComponent,
    children: [
      {
        path: '',
        component: DashboardComponent
      },
      {
        path: 'add-book',
        component: AddBookComponent
      },
      {
        path: 'all-books',
        component: AllBooksComponent
      },
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(route)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule { }
